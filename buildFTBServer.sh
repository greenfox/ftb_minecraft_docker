NAME="ftb_beyondserver"
DOCKER_IMAGE=${USER}/$NAME


#Dockerfile
cat << DOCKERFILE > Dockerfile
FROM ubuntu:18.04

RUN apt-get update
RUN apt-get install -y openjdk-8-jre unzip wget

WORKDIR /FTB

COPY *.zip .

RUN unzip *.zip
RUN rm *.zip
RUN echo "eula=true" > eula.txt #probably should extract this somehow?

EXPOSE 25565

CMD ["bash" , "ServerStart.sh"]
DOCKERFILE
#End Dockerfile!



#build the docker image from above docker file
docker build -t $DOCKER_IMAGE .

#create volume
docker volume create $NAME


#cleanup
rm -f Dockerfile *.zip
