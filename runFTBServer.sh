NAME="ftb_beyondserver"
DOCKER_IMAGE=${USER}/$NAME

#build docker container
docker run --name "ftb_server" --mount source=$NAME,target=/FTB/world -it -p 25565:25565 -d --restart always $DOCKER_IMAGE
